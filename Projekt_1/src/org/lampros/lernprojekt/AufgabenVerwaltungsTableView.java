package org.lampros.lernprojekt;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

public class AufgabenVerwaltungsTableView extends JPanel implements ViewComponent {

	private AufgabenVerwaltungsController controller;
	private AufgabenTableModelAdapter tableAdapter;
	private JTable jTable;
	private JButton neueAufgabe;
	private JButton loeschen;
	private JPanel panel;

	public AufgabenVerwaltungsController getController() {
		return controller;
	}

	public void setController(AufgabenVerwaltungsController controller) {
		this.controller = controller;
	}

	@Override
	public void createView() {

		this.setLayout(new BorderLayout());

		tableAdapter = new AufgabenTableModelAdapter(this.controller);

		this.jTable = new JTable(tableAdapter);
		this.jTable.setBounds(50, 50, 500, 400);
		JScrollPane jScrollPane = new JScrollPane(jTable);
		this.add(jScrollPane, BorderLayout.CENTER);

		panel = new JPanel(new FlowLayout());
		this.panel.setAlignmentX(CENTER_ALIGNMENT);
		this.neueAufgabe = new JButton("Neue Aufgabe");
		this.panel.add(neueAufgabe);
		this.loeschen = new JButton("Loeschen");
		this.panel.add(loeschen);
		this.add(panel, BorderLayout.SOUTH);

		final AufgabenVerwaltungsController detailController = getController();

		// listener auf selection change
		jTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (jTable.getSelectedRow() >= 0) {
					detailController.getModel().setSelectedAufgabe(
							detailController.getModel().getAufgabenVerwaltungsList().get(jTable.getSelectedRow()));
				}
			}
		});

		// reagiere auf �nderungen der TabellenDaten
		detailController.getModel().addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				if ("aufgabenVerwaltungsList".equals(evt.getPropertyName())) {
					tableAdapter.fireTableDataChanged();
				}
			}
		});

		neueAufgabe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				detailController.addNeueAufgabe();
			}
		});

		loeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.showConfirmDialog(null, "Sind Sie sicher?", "Achtung",
						JOptionPane.YES_OPTION);
				if (dialogButton == JOptionPane.YES_OPTION) {
					detailController.deleteAufgabe(jTable.getSelectedRow());
				} else {
					JOptionPane.showMessageDialog(null, "Die Aufabe wurde nicht geloescht");
				}
			}
		});

	}

}

class AufgabenTableModelAdapter extends AbstractTableModel {

	private List<ColumnDefinition> columnNames = new ArrayList<>();
	private AufgabenVerwaltungsController controller;

	public AufgabenTableModelAdapter(AufgabenVerwaltungsController controllerParam) {
		this.controller = controllerParam;
		columnNames.add(new ColumnDefinition("Bezeichnung", String.class));
		columnNames.add(new ColumnDefinition("Prioritat", Integer.class));
		columnNames.add(new ColumnDefinition("Erstellt am", Date.class));
		columnNames.add(new ColumnDefinition("Erledigt", Boolean.class));
	}

	
	
	@Override
	public String getColumnName(int column) {
		return this.columnNames.get(column).name;
	}



	@Override
	public int getColumnCount() {
		return columnNames.size();
	}

	@Override
	public int getRowCount() {
		return this.controller.getModel().getAufgabenVerwaltungsList().size();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnNames.get(columnIndex).type;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		AufgabenDO aufgabe = this.controller.getModel().getAufgabenVerwaltungsList().get(rowIndex);

		if (aufgabe != null) {
			switch (columnIndex) {
			case 0:
				return aufgabe.getBezeichnung();
			case 1:
				return aufgabe.getPrioritat();
			case 2:
				if (aufgabe.getErstellungsDatum() != null) {
					return aufgabe.getErstellungsDatum().getTime();
				}
			case 3:
				return aufgabe.isStatus();
			default:
				break;
			}
		}
		return null;
	}

}

class ColumnDefinition {
	String name;
	Class<?> type;

	ColumnDefinition(String nameParam, Class<?> typeParam) {
		this.name = nameParam;
		this.type = typeParam;
	}

}
