package org.lampros.lernprojekt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AufgabenVerwaltungsDetailPanelView extends JPanel implements ViewComponent {

	private AufgabenVerwaltungsController controller;
	private JButton ubernehmen;
	private JLabel bezeichnungsLabel;
	private JLabel prioritatLabel;
	private JComboBox<Integer> prioritatBox;
	private JLabel datumLabel;
	private JLabel beschreibungsLabel;
	private JLabel erledigtLabel;
	private JTextField bezeichnungsField;
	private JTextField datumField;
	private JTextField beschreibungsField;
	private JCheckBox erledigtCheck;

	public AufgabenVerwaltungsController getController() {
		return controller;
	}

	public void setController(AufgabenVerwaltungsController controller) {
		this.controller = controller;
	}

	@Override
	public void createView() {

		this.setLayout(new GridBagLayout());

		this.bezeichnungsLabel = new JLabel("Bezeichnung");
		GridBagConstraints bezeichnungsLabelGridBagContraints = new GridBagConstraints();
		bezeichnungsLabelGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		bezeichnungsLabelGridBagContraints.weightx = 0.5;
		bezeichnungsLabelGridBagContraints.weighty = 1.0;
		bezeichnungsLabelGridBagContraints.gridx = 0;
		bezeichnungsLabelGridBagContraints.gridy = 0;
		this.add(bezeichnungsLabel, bezeichnungsLabelGridBagContraints);

		this.bezeichnungsField = new JTextField();
		GridBagConstraints bezeichnungsFieldGridBagContraints = new GridBagConstraints();
		bezeichnungsFieldGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		bezeichnungsFieldGridBagContraints.weightx = 0.5;
		bezeichnungsFieldGridBagContraints.weighty = 1.0;
		bezeichnungsFieldGridBagContraints.ipadx = 10;
		bezeichnungsFieldGridBagContraints.gridx = 1;
		bezeichnungsFieldGridBagContraints.gridy = 0;
		this.add(bezeichnungsField, bezeichnungsFieldGridBagContraints);

		this.prioritatLabel = new JLabel("Prioritat");
		GridBagConstraints prioritatLabelGridBagContraints = new GridBagConstraints();
		prioritatLabelGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		prioritatLabelGridBagContraints.weightx = 0.5;
		prioritatLabelGridBagContraints.weighty = 1.0;
		prioritatLabelGridBagContraints.gridx = 0;
		prioritatLabelGridBagContraints.gridy = 1;
		this.add(prioritatLabel, prioritatLabelGridBagContraints);

		Integer[] values = { 1, 2, 3, 4 };
		this.prioritatBox = new JComboBox<>(values);
		GridBagConstraints prioritatBoxGridBagContraints = new GridBagConstraints();
		prioritatBoxGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		prioritatBoxGridBagContraints.weightx = 0.5;
		prioritatBoxGridBagContraints.weighty = 1.0;
		prioritatBoxGridBagContraints.gridx = 1;
		prioritatBoxGridBagContraints.gridy = 1;
		this.add(prioritatBox, prioritatBoxGridBagContraints);

		this.datumLabel = new JLabel("Datum");
		GridBagConstraints datumLabelGridBagContraints = new GridBagConstraints();
		datumLabelGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		datumLabelGridBagContraints.weightx = 0.5;
		datumLabelGridBagContraints.weighty = 1.0;
		datumLabelGridBagContraints.gridx = 0;
		datumLabelGridBagContraints.gridy = 2;
		this.add(datumLabel, datumLabelGridBagContraints);

		this.datumField = new JTextField();
		GridBagConstraints datumFieldGridBagContraints = new GridBagConstraints();
		datumFieldGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		datumFieldGridBagContraints.weightx = 0.5;
		datumFieldGridBagContraints.weighty = 1.0;
		datumFieldGridBagContraints.gridx = 1;
		datumFieldGridBagContraints.gridy = 2;
		this.add(datumField, datumFieldGridBagContraints);

		this.erledigtLabel = new JLabel("Erledigt");
		GridBagConstraints erledigtLabelGridBagContraints = new GridBagConstraints();
		erledigtLabelGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		erledigtLabelGridBagContraints.weightx = 0.5;
		erledigtLabelGridBagContraints.weighty = 1.0;
		erledigtLabelGridBagContraints.gridx = 0;
		erledigtLabelGridBagContraints.gridy = 3;
		this.add(erledigtLabel, erledigtLabelGridBagContraints);

		this.erledigtCheck = new JCheckBox();
		GridBagConstraints erledigtCheckGridBagContraints = new GridBagConstraints();
		erledigtCheckGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		erledigtCheckGridBagContraints.weightx = 0.5;
		erledigtCheckGridBagContraints.weighty = 1.0;
		erledigtCheckGridBagContraints.gridx = 1;
		erledigtCheckGridBagContraints.gridy = 3;
		this.add(erledigtCheck, erledigtCheckGridBagContraints);

		this.beschreibungsLabel = new JLabel("Weitere Angabe");
		GridBagConstraints beschreibungsLabelGridBagContraints = new GridBagConstraints();
		beschreibungsLabelGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		beschreibungsLabelGridBagContraints.weightx = 0.5;
		beschreibungsLabelGridBagContraints.weighty = 1.0;
		beschreibungsLabelGridBagContraints.gridx = 0;
		beschreibungsLabelGridBagContraints.gridy = 4;
		this.add(beschreibungsLabel, beschreibungsLabelGridBagContraints);

		this.beschreibungsField = new JTextField();
		GridBagConstraints beschreibungsFieldGridBagContraints = new GridBagConstraints();
		beschreibungsFieldGridBagContraints.fill = GridBagConstraints.HORIZONTAL;
		beschreibungsFieldGridBagContraints.ipady = 40; // adding height
		beschreibungsFieldGridBagContraints.weightx = 0.5;
		beschreibungsFieldGridBagContraints.weighty = 1.0;
		beschreibungsFieldGridBagContraints.gridx = 1;
		beschreibungsFieldGridBagContraints.gridy = 4;
		this.add(beschreibungsField, beschreibungsFieldGridBagContraints);

		this.ubernehmen = new JButton("Ubernehmen");
		GridBagConstraints ubernehmenGridBagContraints = new GridBagConstraints();
		ubernehmenGridBagContraints.weightx = 0.5;
		ubernehmenGridBagContraints.weighty = 1.0;
		ubernehmenGridBagContraints.gridx = 0;
		ubernehmenGridBagContraints.gridy = 5;
		this.add(ubernehmen, ubernehmenGridBagContraints);

		final AufgabenVerwaltungsController detailController = getController();

		detailController.getModel().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) { // reagiere auf Anderungen der TabellenDaten
				if ("selectedAufgabe".equals(evt.getPropertyName())) {
					if (detailController.getModel().getSelectedAufgabe() != null) {
						updateValuesToControl(detailController.getModel().getSelectedAufgabe());
					} else {
						clearValuesFromControl();
					}
				}
			}
		});

		ubernehmen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.showConfirmDialog(null, "Sind Sie sicher?", "Achtung",
						JOptionPane.YES_OPTION);
				if (dialogButton == JOptionPane.YES_OPTION) {
					try {
						AufgabenDO aufgabe = detailController.getModel().getSelectedAufgabe();

						if (aufgabe != null) {
							updateValuesToModel(aufgabe);
							detailController.speichernAufgabe(aufgabe);
							clearValuesFromControl();
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} else
					JOptionPane.showMessageDialog(null, "Die neue Aufgabe wurde nicht gespeichert");
			}
		});
	}

	private void updateValuesToControl(AufgabenDO aufgabe) {
		bezeichnungsField.setText(aufgabe.getBezeichnung());
		erledigtCheck.setSelected(aufgabe.isStatus());
		beschreibungsField.setText(aufgabe.getBeschreibung());
		datumField.setText(CreateAufgabe.getDateAsString(aufgabe.getErstellungsDatum()));
		prioritatBox.setSelectedItem(aufgabe.getPrioritat());
	}

	private void updateValuesToModel(AufgabenDO aufgabe) throws ParseException {
		aufgabe.setBezeichnung(bezeichnungsField.getText());
		aufgabe.setStatus(erledigtCheck.isSelected());
		aufgabe.setBeschreibung(beschreibungsField.getText());
		aufgabe.setErstellungsDatum(CreateAufgabe.getDateAsCalendar(datumField.getText()));
		aufgabe.setPrioritat((Integer) prioritatBox.getSelectedItem());
	}

	private void clearValuesFromControl() {
		bezeichnungsField.setText("");
		prioritatBox.setSelectedItem(1);
		datumField.setText("");
		erledigtCheck.setSelected(false);
		beschreibungsField.setText("");
	}
}
