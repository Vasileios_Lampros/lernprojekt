package org.lampros.lernprojekt;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

public class AufgabenVerwaltungsModel {

	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	private List<AufgabenDO> aufgabenVerwaltungsList = new ArrayList<>();
		private AufgabenDO selectedAufgabe;
		
		public List<AufgabenDO> getAufgabenVerwaltungsList() {
			return aufgabenVerwaltungsList;
		}
		
		public void addToAufgabenVerwaltungsList(AufgabenDO aufgabe) {
			if (this.aufgabenVerwaltungsList.contains(aufgabe)) return; //liste enth�lt aufgabe schon, kein 2tes Mal hinzuf�gen
			this.aufgabenVerwaltungsList.add(aufgabe);
			this.selectedAufgabe = aufgabe;
			
			this.pcs.firePropertyChange(new PropertyChangeEvent(this, "aufgabenVerwaltungsList", null, aufgabe));
			this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selectedAufgabe", null, this.selectedAufgabe));
		}
		
		public void updateAufgabenVerwaltungsList(AufgabenDO aufgabe) {
			this.pcs.firePropertyChange(new PropertyChangeEvent(this, "aufgabenVerwaltungsList", null, aufgabe));
		}
		
		public void deleteFromAufgabenVerwaltungsList(AufgabenDO aufgabe) {
			this.aufgabenVerwaltungsList.remove(aufgabe);
			
			this.pcs.firePropertyChange(new PropertyChangeEvent(this, "aufgabenVerwaltungsList", aufgabe, null));
		}
		
		public AufgabenDO getSelectedAufgabe() {
			return selectedAufgabe;
		}
		public void setSelectedAufgabe(AufgabenDO selectedAufgabe) {
			AufgabenDO oldValue = this.selectedAufgabe;
			this.selectedAufgabe = selectedAufgabe;
			
			this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selectedAufgabe", oldValue, this.selectedAufgabe));
		}

}
