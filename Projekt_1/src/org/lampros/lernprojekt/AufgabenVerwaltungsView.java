package org.lampros.lernprojekt;

import java.awt.BorderLayout;
import java.awt.HeadlessException;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AufgabenVerwaltungsView extends JFrame implements ViewComponent {

	private AufgabenVerwaltungsController controller;
	private JLabel titelLabel;

	public AufgabenVerwaltungsView() throws HeadlessException {
		super("Aufgaben Verwaltung");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public AufgabenVerwaltungsController getController() {
		return controller;
	}

	public void setController(AufgabenVerwaltungsController controller) {
		this.controller = controller;
	}

	public void showView() {
		this.pack();
		this.setVisible(true);
		this.setSize(500, 400);
	}

	public void exitView() {
		this.setVisible(false);
	}

	@Override
	public void createView() {
		Box box = Box.createVerticalBox();
		
		this.titelLabel = new JLabel("Vorhandene Aufgaben");
		box.add(titelLabel);

		AufgabenVerwaltungsTableView tableView = new AufgabenVerwaltungsTableView();
		tableView.setController(this.controller);
		tableView.createView();
		box.add(tableView);

		AufgabenVerwaltungsDetailPanelView detailPanel = new AufgabenVerwaltungsDetailPanelView();
		detailPanel.setController(this.controller);
		detailPanel.createView();
		box.add(detailPanel);
		
		this.add(box);
	}
}
