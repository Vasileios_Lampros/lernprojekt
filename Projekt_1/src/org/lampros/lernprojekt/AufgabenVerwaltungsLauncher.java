package org.lampros.lernprojekt;

public class AufgabenVerwaltungsLauncher {

	AufgabenVerwaltungsController controller;
	

	public static void main(String[] args) {

		AufgabenVerwaltungsLauncher launcher = new AufgabenVerwaltungsLauncher();
		launcher.startApp();
	}

	public void startApp() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowView();
			}
		});
	}

	private void createAndShowView() {
		controller = new AufgabenVerwaltungsController(new AufgabenVerwaltungsModel(), new AufgabenVerwaltungsView());
		controller.showView();
	}
}
