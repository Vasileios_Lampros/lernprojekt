package org.lampros.lernprojekt;
import java.util.Calendar;

public class AufgabenDO {

	private String bezeichnung;
	private Integer prioritat;
	private Calendar erstellungsDatum;
	private boolean erledigt = false;
	private String beschreibung;
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public Calendar getErstellungsDatum() {
		return erstellungsDatum;
	}
	public void setErstellungsDatum(Calendar erstelltAm) {
		this.erstellungsDatum = erstelltAm;
	}
	
	public boolean isStatus() {
		return erledigt;
	}
	public void setStatus(boolean erledigt) {
		this.erledigt = erledigt;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	public Integer getPrioritat() {
		return prioritat;
	}
	public void setPrioritat(Integer priority) {
		this.prioritat = priority;
	}

}
