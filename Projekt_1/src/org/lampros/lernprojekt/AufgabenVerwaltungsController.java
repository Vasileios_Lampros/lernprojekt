package org.lampros.lernprojekt;

import java.util.ArrayList;
import java.util.List;

public class AufgabenVerwaltungsController {

	private AufgabenVerwaltungsModel model;
	private AufgabenVerwaltungsView view;

	public AufgabenVerwaltungsController(AufgabenVerwaltungsModel modelParam, AufgabenVerwaltungsView viewParam) {
		assert viewParam != null;
		assert modelParam != null;

		this.model = modelParam;
		this.view = viewParam;
		this.view.setController(this);
	}

	public AufgabenVerwaltungsModel getModel() {
		return model;
	}

	public void showView() {
		ladeAufgaben();
		this.view.createView();
		this.view.showView();
	}

	public void ladeAufgaben() {
		this.model.getAufgabenVerwaltungsList().addAll(getInitialData());
	}

	public List<AufgabenDO> getInitialData() {
		List<AufgabenDO> initialData = new ArrayList<>();
		initialData.add(CreateAufgabe.createAufgabenDO("Schreiben", 1, "Bezeichnung"));
		initialData.add(CreateAufgabe.createAufgabenDO("Wiederholung", 2, "Bezeichnung"));
		initialData.add(CreateAufgabe.createAufgabenDO("HeimArbeit", 3, "Bezeichnung"));
		initialData.add(CreateAufgabe.createAufgabenDO("Test", 4, "Bezeichnung"));

		return initialData;
	}

	public void addNeueAufgabe() {
		AufgabenDO aufgabe = CreateAufgabe.createAufgabenDO(null, null, null);
		this.model.addToAufgabenVerwaltungsList(aufgabe);
	}

	public void deleteAufgabe(int index) {
		this.model.deleteFromAufgabenVerwaltungsList(this.getModel().getAufgabenVerwaltungsList().remove(index));
	}

	public void speichernAufgabe(AufgabenDO aufgabe) {
		this.model.updateAufgabenVerwaltungsList(aufgabe);
		this.model.setSelectedAufgabe(null);
	}

}
