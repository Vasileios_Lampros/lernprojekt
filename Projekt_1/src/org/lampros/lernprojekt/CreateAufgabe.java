package org.lampros.lernprojekt;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CreateAufgabe {

	public static AufgabenDO createAufgabenDO(String bezeichnung, Integer prio, String beschreibung) {
		AufgabenDO aufgabe = new AufgabenDO();
		aufgabe.setBezeichnung(bezeichnung);
		aufgabe.setPrioritat(prio);
		aufgabe.setErstellungsDatum(Calendar.getInstance());
		aufgabe.setBeschreibung(beschreibung);

		return aufgabe;
	}

	public static String getDateAsString(Calendar date) {
		return getDateAsString(date, "dd.MM.yyyy");
	}

	public static String getDateAsString(Calendar date, String formatPattern) {
		if (date == null)
			return "";

		SimpleDateFormat df = new SimpleDateFormat(formatPattern);
		String formattedDate = df.format(date.getTime());

		return formattedDate;
	}

	public static Calendar getDateAsCalendar(String dateString) throws ParseException {
		return getDateAsCalendar(dateString, "dd.MM.yyyy");
	}

	public static Calendar getDateAsCalendar(String dateString, String formatPattern) throws ParseException {
		if (dateString == null)
			return null;

		SimpleDateFormat df = new SimpleDateFormat(formatPattern);
		Date date = df.parse(dateString);

		return getCurrentCalendarFromDate(date);
	}

	public static Calendar getCurrentCalendarFromDate(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(date.getTime());
		return cal;
	}
}
