package org.oss4b.samples.todo.view;

import java.util.ArrayList;
import java.util.List;

import org.oss4b.samples.todo.data.AufgabeDO;
import org.oss4b.samples.todo.data.AufgabeService;

public class AufgabenverwaltungController {
	private AufgabenverwaltungModel model;
	private AufgabenverwaltungView view;
	
	public AufgabenverwaltungController(AufgabenverwaltungView viewParam, AufgabenverwaltungModel modelParam) {
		assert viewParam != null;
		assert modelParam != null;
		
		this.view = viewParam;
		this.model = modelParam;
		
		this.view.setController(this);
	}

	public AufgabenverwaltungModel getModel() {
		return model;
	}
	
	public void showView() {
		//1. lade die Daten aus dem Datastore
		ladeAufgaben();
		
		//2. erzeuge View
		this.view.createView();
		
		//3. zeige die View an
		this.view.showView();
	}
	
	public void ladeAufgaben() {
		this.model.getAlleAufgaben().addAll(getSampleData());
	}
	
	private List<AufgabeDO> getSampleData() {
		List<AufgabeDO> data = new ArrayList<>();
		data.add(AufgabeService.createAufgabe("Aufgabe 1", 1, "Beschreibung der Aufgabe 1"));
		data.add(AufgabeService.createAufgabe("Aufgabe 2", 4, "Beschreibung der Aufgabe 2"));
		data.add(AufgabeService.createAufgabe("Aufgabe 3", 2, "Beschreibung der Aufgabe 3"));
		data.add(AufgabeService.createAufgabe("Aufgabe 4", 3, "Beschreibung der Aufgabe 4"));
		
		return data;
	}

	public void neueAufgabe() {
		AufgabeDO aufgabe = AufgabeService.createAufgabe(null, null, null);
		this.model.addAufgabeToAlleAufgaben(aufgabe);
	}
	
	public void deleteAufgabe(int indexToDelete) {
		this.model.deleteAufgabeFromAlleAufgaben(this.getModel().getAlleAufgaben().get(indexToDelete));
	}
	
	public void speichereAufgabe(AufgabeDO aufgabe) {
		this.model.updateAufgabeToAlleAufgaben(aufgabe);
		this.model.setSelektierteAufgabe(null);
	}
	
	public void cancelAufgabe(AufgabeDO aufgabe) {
		this.model.setSelektierteAufgabe(null);		
	}
}
