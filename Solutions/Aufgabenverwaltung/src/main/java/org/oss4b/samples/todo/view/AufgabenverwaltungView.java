package org.oss4b.samples.todo.view;

import java.awt.BorderLayout;
import java.awt.HeadlessException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;

public class AufgabenverwaltungView extends JFrame implements ViewComponent {
	private AufgabenverwaltungController controller;
	
	public AufgabenverwaltungView() throws HeadlessException {
		super("Aufgaben-Verwaltung");
 		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public AufgabenverwaltungController getController() {
		return controller;
	}
	public void setController(AufgabenverwaltungController controller) {
		this.controller = controller;
	}


	public void showView() {
 		this.pack();
 		this.setVisible(true);		
	}
	
	public void exitView() {
 		this.setVisible(false);
	}

	@Override
	public void createView() {
		Box box = Box.createVerticalBox();
		this.getContentPane().add(box, BorderLayout.CENTER);
		
		AufgabenverwaltungTableView master = new AufgabenverwaltungTableView();
		master.setController(this.controller);
		master.createView();
		box.add(master);
		
		AufgabenverwaltungDetailView detail = new AufgabenverwaltungDetailView();
		detail.setController(this.controller);
		detail.createView();		
		box.add(detail);
	}
	
}
