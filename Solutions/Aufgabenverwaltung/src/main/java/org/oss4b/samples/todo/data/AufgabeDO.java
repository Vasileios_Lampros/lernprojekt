package org.oss4b.samples.todo.data;

import java.util.Calendar;

public class AufgabeDO {

	private String bezeichnung;
	private Integer priority;
	private Calendar erstelltAm;
	private Calendar beendetAm;
	private Calendar zuletztGeaendertAm;
	private boolean erledigt = false;
	private String beschreibung;
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public Calendar getErstelltAm() {
		return erstelltAm;
	}
	public void setErstelltAm(Calendar erstelltAm) {
		this.erstelltAm = erstelltAm;
	}
	public Calendar getBeendetAm() {
		return beendetAm;
	}
	public void setBeendetAm(Calendar beendetAm) {
		this.beendetAm = beendetAm;
	}
	public Calendar getZuletztGeaendertAm() {
		return zuletztGeaendertAm;
	}
	public void setZuletztGeaendertAm(Calendar zuletztGeaendertAm) {
		this.zuletztGeaendertAm = zuletztGeaendertAm;
	}
	public boolean isErledigt() {
		return erledigt;
	}
	public void setErledigt(boolean erledigt) {
		this.erledigt = erledigt;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	
}
