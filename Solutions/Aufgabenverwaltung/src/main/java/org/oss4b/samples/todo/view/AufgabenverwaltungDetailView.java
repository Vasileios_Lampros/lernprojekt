package org.oss4b.samples.todo.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.oss4b.samples.todo.data.AufgabeDO;
import org.oss4b.samples.todo.data.AufgabeService;

public class AufgabenverwaltungDetailView extends JPanel implements ViewComponent {
	private AufgabenverwaltungController controller;
	
	//fields
	private JTextField bezeichnungField;
	private JTextArea beschreibungField;
	private JTextField erstelltAmField;
	private JComboBox<Integer> priorityField;
	private JCheckBox erledigtField; 
	
	
	public AufgabenverwaltungController getController() {
		return controller;
	}


	public void setController(AufgabenverwaltungController controller) {
		this.controller = controller;
	}
	
	@Override
	public void createView() {
		this.setLayout(new BorderLayout());
		
		JPanel detailControls = new JPanel();
		detailControls.setLayout(new GridLayout(0,2));
		this.add(detailControls, BorderLayout.CENTER);
		
		JLabel bezeichnungLabel = new JLabel("Bezeichnung");
		detailControls.add(bezeichnungLabel);
		bezeichnungField = new JTextField(20);
		detailControls.add(bezeichnungField);

		Integer[] prios = {1,2,3,4};
		JLabel prioLabel = new JLabel("Priorität");
		detailControls.add(prioLabel);
		priorityField = new JComboBox<>(prios);
		detailControls.add(priorityField);

		JLabel erstelltLabel = new JLabel("Erstellt am");
		detailControls.add(erstelltLabel);
		erstelltAmField = new JTextField(20);
		detailControls.add(erstelltAmField);

		JLabel erledigtLabel = new JLabel("Erledigt");
		detailControls.add(erledigtLabel);
		erledigtField = new JCheckBox();
		detailControls.add(erledigtField);

		JLabel beschreibungLabel = new JLabel("Beschreibung");
		detailControls.add(beschreibungLabel);
		beschreibungField = new JTextArea();
		detailControls.add(beschreibungField);
		
		
		final AufgabenverwaltungController detailController = getController();
		
		//reagiere auf Änderungen der TabellenDaten
		detailController.getModel().addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("selektierteAufgabe".equals(evt.getPropertyName())) {
					if (detailController.getModel().getSelektierteAufgabe() != null) {
						updateValuesToControl(detailController.getModel().getSelektierteAufgabe());
					} else {
						clearValuesFromControl();
					}
				}
			}
		});
				
		Box buttons = Box.createHorizontalBox();
		this.add(buttons, BorderLayout.SOUTH);
		
 		JButton speichereAufgabe = new JButton("Speichern");
 		buttons.add(speichereAufgabe);
 		
 		speichereAufgabe.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					AufgabeDO aufgabe = detailController.getModel().getSelektierteAufgabe();
					
					if (aufgabe != null) {
						updateValuesToModel(aufgabe);
						detailController.speichereAufgabe(aufgabe);
						clearValuesFromControl();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					//TODO : nachrüsten von wirklichem Fehlerlogging + Fehlerdialog
				}
			}
		});

 		JButton cancelAufgabe = new JButton("Abbrechen");
 		buttons.add(cancelAufgabe);
 		
 		cancelAufgabe.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//kein update aus controls
				detailController.cancelAufgabe(detailController.getModel().getSelektierteAufgabe());
				clearValuesFromControl();
			}
		});

	}

	private void updateValuesToControl(AufgabeDO aufgabe) {
		bezeichnungField.setText(aufgabe.getBezeichnung());
		erledigtField.setSelected(aufgabe.isErledigt());
		beschreibungField.setText(aufgabe.getBeschreibung());
		erstelltAmField.setText(AufgabeService.getDateAsString(aufgabe.getErstelltAm()));
		priorityField.setSelectedItem(aufgabe.getPriority());
	}
	
	private void updateValuesToModel(AufgabeDO aufgabe) throws ParseException {
		aufgabe.setBezeichnung(bezeichnungField.getText());
		aufgabe.setErledigt(erledigtField.isSelected());
		aufgabe.setBeschreibung(beschreibungField.getText());
		aufgabe.setErstelltAm(AufgabeService.getDateAsCalendar(erstelltAmField.getText()));
		aufgabe.setPriority((Integer)priorityField.getSelectedItem());
	}
	
	private void clearValuesFromControl() {
		bezeichnungField.setText("");
		erledigtField.setSelected(false);
		beschreibungField.setText("");
		erstelltAmField.setText("");
		priorityField.setSelectedItem(1);
	}
}
