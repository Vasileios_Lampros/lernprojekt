package org.oss4b.samples.todo.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.oss4b.samples.todo.data.AufgabeDO;

public class AufgabenverwaltungTableView extends JPanel implements ViewComponent {
	private AufgabenverwaltungController controller;

	//components
	private JTable table;
	private AufgabenTableModelAdapter tableAdapter;
	
	public AufgabenverwaltungController getController() {
		return controller;
	}

	public void setController(AufgabenverwaltungController controller) {
		this.controller = controller;
	}

	@Override
	public void createView() {
		this.setLayout(new BorderLayout());
		
		tableAdapter = new AufgabenTableModelAdapter(this.controller);

		table = new JTable(tableAdapter);
		table.setBounds(50, 50, 500, 400);
		JScrollPane jScrollPane = new JScrollPane(table);
		this.add(jScrollPane, BorderLayout.CENTER);

		final AufgabenverwaltungController detailController = getController();

		//listener auf selection change
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (table.getSelectedRow() >= 0) {
					detailController.getModel().setSelektierteAufgabe(detailController.getModel().getAlleAufgaben().get(table.getSelectedRow()));					
				}
			}
		});
		
		
		//reagiere auf �nderungen der TabellenDaten
		detailController.getModel().addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("alleAufgaben".equals(evt.getPropertyName())) {
					tableAdapter.fireTableDataChanged();
				}
			}
		});
		
		//erzeuge Buttons mit Action-Delegates
		Box buttons = Box.createHorizontalBox();
		this.add(buttons, BorderLayout.SOUTH);
		
 		JButton neueAufgabe = new JButton("Neue Aufgabe");
 		buttons.add(neueAufgabe);
 		
 		neueAufgabe.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				detailController.neueAufgabe();
			}
		});

 		JButton deleteAufgabe = new JButton("L�schen der Aufgabe");
 		buttons.add(deleteAufgabe);
 		
 		deleteAufgabe.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				detailController.deleteAufgabe(table.getSelectedRow());
			}
		});

	}

}

class AufgabenTableModelAdapter extends AbstractTableModel {
	private List<ColumnDefinition> columns = new ArrayList<>();

	private AufgabenverwaltungController controller;

	AufgabenTableModelAdapter(AufgabenverwaltungController controllerParam) {
		this.controller = controllerParam;

		// init data
		columns.add(new ColumnDefinition("Bezeichnung", String.class));
		columns.add(new ColumnDefinition("Priorit�t", Integer.class));
		columns.add(new ColumnDefinition("Erstellt am", Date.class));
		columns.add(new ColumnDefinition("Erledigt", Boolean.class));
	}

	@Override
	public int getRowCount() {
		return this.controller.getModel().getAlleAufgaben().size();
	}

	@Override
	public int getColumnCount() {
		return columns.size();
	}

	@Override
	public String getColumnName(int column) {
		return columns.get(column).name;
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columns.get(columnIndex).type;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		AufgabeDO aufgabe = this.controller.getModel().getAlleAufgaben().get(rowIndex);
		
		if (aufgabe != null) {
			switch (columnIndex) {
			case 0:
				return aufgabe.getBezeichnung();

			case 1:
				return aufgabe.getPriority();

			case 2:
				if (aufgabe.getErstelltAm() != null) {
					return aufgabe.getErstelltAm().getTime();
				}

			case 3:
				return aufgabe.isErledigt();

			default:
				break;
			}
		}
		
		return null;
	}

}

class ColumnDefinition {
	String name;
	Class<?> type;

	ColumnDefinition(String nameParam, Class<?> typeParam) {
		this.name = nameParam;
		this.type = typeParam;
	}
}