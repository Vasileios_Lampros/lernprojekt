package org.oss4b.samples.todo.view;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import org.oss4b.samples.todo.data.AufgabeDO;

public class AufgabenverwaltungModel {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.pcs.removePropertyChangeListener(listener);
    }

	private List<AufgabeDO> alleAufgaben = new ArrayList<>();
	private AufgabeDO selektierteAufgabe;
	
	public List<AufgabeDO> getAlleAufgaben() {
		return alleAufgaben;
	}
	
	public void addAufgabeToAlleAufgaben(AufgabeDO aufgabe) {
		if (this.alleAufgaben.contains(aufgabe)) return; //liste enth�lt aufgabe schon, kein 2tes Mal hinzuf�gen
		this.alleAufgaben.add(aufgabe);
		this.selektierteAufgabe = aufgabe;
		
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "alleAufgaben", null, aufgabe));
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selektierteAufgabe", null, this.selektierteAufgabe));
	}
	
	public void updateAufgabeToAlleAufgaben(AufgabeDO aufgabe) {
		//nur feuern, da liste aufgabe schon enth�lt
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "alleAufgaben", null, aufgabe));
	}
	
	public void deleteAufgabeFromAlleAufgaben(AufgabeDO aufgabe) {
		if (!this.alleAufgaben.contains(aufgabe)) return; //liste enth�lt aufgabe nicht
		this.alleAufgaben.remove(aufgabe);
		
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "alleAufgaben", aufgabe, null));
	}
	
	public AufgabeDO getSelektierteAufgabe() {
		return selektierteAufgabe;
	}
	public void setSelektierteAufgabe(AufgabeDO selektierteAufgabe) {
		AufgabeDO oldValue = this.selektierteAufgabe;
		this.selektierteAufgabe = selektierteAufgabe;
		
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selektierteAufgabe", oldValue, this.selektierteAufgabe));
	}
	
	
}
