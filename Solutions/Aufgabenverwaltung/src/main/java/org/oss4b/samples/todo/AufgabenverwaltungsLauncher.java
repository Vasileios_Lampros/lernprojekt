package org.oss4b.samples.todo;

import org.oss4b.samples.todo.view.AufgabenverwaltungController;
import org.oss4b.samples.todo.view.AufgabenverwaltungModel;
import org.oss4b.samples.todo.view.AufgabenverwaltungView;

public class AufgabenverwaltungsLauncher {
	AufgabenverwaltungController controller;
			
	public static void main(String[] args) {

		AufgabenverwaltungsLauncher launcher = new AufgabenverwaltungsLauncher();
		launcher.startApplication();

	}

	public void startApplication() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowView();
			}
		});
	}

	private void createAndShowView() {
		controller = new AufgabenverwaltungController(new AufgabenverwaltungView(), new AufgabenverwaltungModel());
		controller.showView();
	}
}
