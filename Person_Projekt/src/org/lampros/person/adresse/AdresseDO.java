package org.lampros.person.adresse;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.lampros.person.data.PersonDO;

@Entity
@Table(name = "ADRESSE")
public class AdresseDO {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ADRESSE_ID", nullable = false)
	private int id;
	@ManyToOne()
	@JoinColumn(name = "OWNER_ID")
	private PersonDO adresseBesitzer;
	@Column(name = "BEZEICHNUNG", nullable = false)
	private String adresseBezeichnung;
	@Column(name = "STRASSE", nullable = false)
	private String strasse;
	@Column(name = "NUMMER", nullable = false)
	private String nummer;
	@Column(name = "PLZ", nullable = false)
	private String postleitzahl;
	@Column(name = "ORT", nullable = false)
	private String ort;

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getNummer() {
		return nummer;
	}

	public void setNummer(String nummer) {
		this.nummer = nummer;
	}

	public String getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(String postleitzahl) {
		this.postleitzahl = postleitzahl;
	}

	public String getAdresseBezeichnung() {
		return adresseBezeichnung;
	}

	public void setAdresseBezeichnung(String adresseBezeichnung) {
		this.adresseBezeichnung = adresseBezeichnung;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public int getAdresseId() {
		return id;
	}

	public void setAdresseId(int id) {
		this.id = id;
	}

	public PersonDO getAdresseBesitzer() {
		return adresseBesitzer;
	}

	public void setAdresseBesitzer(PersonDO adresseBesitzer) {
		this.adresseBesitzer = adresseBesitzer;
		if (!adresseBesitzer.getPersonAdresseList().contains(this)) {
			adresseBesitzer.getPersonAdresseList().add(this);
		}
	}

	public static AdresseDO createAdresseDO(String adresseBezeichnungParam, String strasseParam,
			String hausnummerParam) {

		AdresseDO adresse = new AdresseDO();
		adresse.setAdresseBezeichnung(adresseBezeichnungParam);
		adresse.setStrasse(strasseParam);
		adresse.setNummer(hausnummerParam);

		return adresse;
	}
}
