package org.lampros.person.adresse;

import javax.swing.table.AbstractTableModel;

import org.lampros.person.data.PersonDO;

public class AdresseTableModel extends AbstractTableModel {
	//TODO: Ist zwar korrekt, aber wir haben es bei PersonView anders strukturiert.
	//Es ist zwingend, dass man sich an einen Stil h�lt und nicht nach tageslaune was anders macht
	//bei grossen Projekten f�hrt dies zu fehlender �bersicht.
	private String[] columnNames = { "Bezeichnung", "Strasse", "Nummer", "PLZ", "Ort" };
	private AdresseController adresseController;

	public AdresseTableModel(AdresseController adresseController) {
		this.adresseController = adresseController;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.adresseController.getAdresseModel().getAdresseList().size();
	}

	@Override
	public Object getValueAt(int row, int column) {

		AdresseDO adresse = this.adresseController.getAdresseModel().getAdresseList().get(row);

		if (adresse != null) {
			switch (column) {
			case 0:
				return adresse.getAdresseBezeichnung();
			case 1:
				return adresse.getStrasse();
			case 2:
				return adresse.getNummer();
			case 3:
				return adresse.getPostleitzahl();
			case 4:
				return adresse.getOrt();
			default:
				break;
			}
		}
		return null;
	}

	@Override
	public void setValueAt(Object newValue, int row, int column) {
		
		AdresseDO adresse = this.adresseController.getAdresseModel().getAdresseList().get(row);

		if (adresse != null) {
			switch (column) {
			case 0:
				adresse.setAdresseBezeichnung((String) newValue);
			case 1:
				adresse.setStrasse((String) newValue);
			case 2:
				adresse.setNummer((String) newValue);
			case 3:
				adresse.setOrt((String) newValue);
			case 4:
				adresse.setPostleitzahl((String) newValue);
			default:
				break;
			}
		}
	}
}