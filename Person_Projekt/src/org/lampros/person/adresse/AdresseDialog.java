package org.lampros.person.adresse;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AdresseDialog extends JFrame {

	private AdresseController adresseController;
	private JLabel bezeichnungsLabel;
	private JTextField bezeichnungField;
	private JLabel strasseLabel;
	private JTextField strasseField;
	private JLabel nummerLabel;
	private JTextField nummerField;
	private JLabel plzLabel;
	private JTextField plzField;
	private JLabel ortLabel;
	private JTextField ortField;
	private JButton okButton;
	private JButton cancelButton;
	private JPanel buttonPanel;
	private JPanel detailPanel;

	public AdresseDialog() {
		this.setTitle("Adresse Details");
		this.setBounds(250, 200, 350, 200);
		this.setVisible(true);
	}

	public void exitView() {
		this.setVisible(false);
	}

	public AdresseController getAdresseController() {
		return adresseController;
	}

	public void setAdresseController(AdresseController adresseController) {
		this.adresseController = adresseController;
	}

	public JTextField getBezeichnungField() {
		return bezeichnungField;
	}

	public JTextField getStrasseField() {
		return strasseField;
	}

	public JTextField getNummerField() {
		return nummerField;
	}

	public JTextField getPlzField() {
		return plzField;
	}

	public JTextField getOrtField() {
		return ortField;
	}

	public void adresseDialogView() {

		this.setLayout(new BorderLayout());
		this.detailPanel = new JPanel(new GridLayout(0, 2));
		this.bezeichnungsLabel = new JLabel("Bezeichnung");
		this.bezeichnungField = new JTextField();
		this.strasseLabel = new JLabel("Strasse");
		this.strasseField = new JTextField();
		this.nummerLabel = new JLabel("Nummer");
		this.nummerField = new JTextField();
		this.plzLabel = new JLabel("PLZ");
		this.plzField = new JTextField();
		this.ortLabel = new JLabel("Ort");
		this.ortField = new JTextField();

		this.detailPanel.add(bezeichnungsLabel);
		this.detailPanel.add(bezeichnungField);
		this.detailPanel.add(strasseLabel);
		this.detailPanel.add(strasseField);
		this.detailPanel.add(nummerLabel);
		this.detailPanel.add(nummerField);
		this.detailPanel.add(plzLabel);
		this.detailPanel.add(plzField);
		this.detailPanel.add(ortLabel);
		this.detailPanel.add(ortField);
		this.add(detailPanel, BorderLayout.NORTH);

		this.buttonPanel = new JPanel();
		this.okButton = new JButton("OK");
		this.cancelButton = new JButton("Cancel");
		this.buttonPanel.add(okButton);
		this.buttonPanel.add(cancelButton);
		this.add(buttonPanel, BorderLayout.SOUTH);

		final AdresseController adresseDialogController = getAdresseController();

		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				AdresseDO adresse = adresseDialogController.getAdresseModel().getSelAdresse();

				if (adresse != null) {
					saveDialogDetails(adresse);
					adresseDialogController.saveAdresse(adresse);
				}
				exitView();
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clearDetails();
				adresseDialogController
						.deleteAdresse(adresseDialogController.getAdresseView().getAdresseTable().getRowCount() - 1);
				exitView();
			}
		});
	}

	public void saveDialogDetails(AdresseDO adresse) {
		adresse.setAdresseBezeichnung(bezeichnungField.getText());
		adresse.setStrasse(strasseField.getText());
		adresse.setNummer(nummerField.getText());
		adresse.setPostleitzahl(plzField.getText());
		adresse.setOrt(ortField.getText());
	}

	public void showAdresseDetails(AdresseDO adresse) {
		bezeichnungField.setText(adresse.getAdresseBezeichnung());
		strasseField.setText(adresse.getStrasse());
		nummerField.setText(adresse.getNummer());
		plzField.setText(adresse.getPostleitzahl());
		ortField.setText(adresse.getOrt());
	}

	private void clearDetails() {
		bezeichnungField.setText("");
		strasseField.setText("");
		nummerField.setText("");
		plzField.setText("");
		ortField.setText("");
	}
}