package org.lampros.person.adresse;

import java.util.List;

import org.lampros.person.data.PersonDAOImpl;
import org.lampros.person.data.PersonDO;
import org.lampros.person.view.PersonController;
import org.lampros.person.view.ViewInteractingInterface;

public class AdresseController implements ViewInteractingInterface {

	private AdresseModel adresseModel;
	private AdresseView adresseView;
	private PersonController personController;

	public AdresseController(AdresseModel adresseModel, AdresseView adresseView) {
		this.adresseModel = adresseModel;
		this.adresseView = adresseView;
		this.adresseView.setAdresseController(this);
	}

	@Override
	public void createView() {
		this.adresseView.adresseMakeView();
	}

	public PersonController getPersonController() {
		return personController;
	}

	public void setPersonController(PersonController personController) {
		this.personController = personController;
	}

	public AdresseModel getAdresseModel() {
		return adresseModel;
	}

	public void setAdresseModel(AdresseModel adresseModel) {
		this.adresseModel = adresseModel;
	}

	public AdresseView getAdresseView() {
		return adresseView;
	}

	public void setAdresseView(AdresseView adresseView) {
		this.adresseView = adresseView;
	}
	
	public AdresseDO addAdresse() {
		
		PersonDO owner = this.personController.getPersonModel().getSelectedPerson();
		
		AdresseDO adresse = AdresseDO.createAdresseDO(null, null, null);
		this.adresseModel.addAdresseToList(adresse, owner);
		return adresse;
	}
	
	public void retrieveAdresseFromDB(PersonDO person) {
		
		PersonDAOImpl dao = new PersonDAOImpl();
		List<AdresseDO> adresseList = dao.retrieveAdresse(person);
		
		for(int i = 0; i < adresseList.size(); i++) {
			this.adresseView.getAdresseTableModel().setValueAt(adresseList.get(i).getAdresseBezeichnung(), i, 0);
			this.adresseView.getAdresseTableModel().setValueAt(adresseList.get(i).getStrasse(), i, 1);
			this.adresseView.getAdresseTableModel().setValueAt(adresseList.get(i).getNummer(), i, 2);
			this.adresseView.getAdresseTableModel().setValueAt(adresseList.get(i).getPostleitzahl(), i, 3);
			this.adresseView.getAdresseTableModel().setValueAt(adresseList.get(i).getOrt(), i, 4);
			
			this.adresseModel.updateAdresseList(adresseList.get(i));
		}
	}

	public void saveAdresse(AdresseDO adresse) {
		this.adresseModel.updateAdresseList(adresse);
	}
	
	public void updateAdresseDetails(AdresseDO adresse) {
		PersonDAOImpl dao = new PersonDAOImpl();
		if (adresse.getAdresseId() == 0) {
			dao.addAdresse(adresse);
		} else {
			dao.updateAdresse(adresse);
		}
	}

	public void deleteAdresse(int adresseIndex) {
		this.adresseModel.deleteAdresseFromList(this.adresseModel.getAdresseList().get(adresseIndex));
	}
	
	public void deleteAdresseFromDB(){
		PersonDAOImpl dao = new PersonDAOImpl();
		dao.deleteAdresse(this.adresseModel.getSelAdresse().getAdresseId());
	}
}