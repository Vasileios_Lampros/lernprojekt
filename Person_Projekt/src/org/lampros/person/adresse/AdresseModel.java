package org.lampros.person.adresse;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import org.lampros.person.data.PersonDO;

public class AdresseModel {

	private AdresseDO selAdresse;
	//TODO : warum brauchen wir noch die this.adresseList ????
	private List<AdresseDO> adresseList = new ArrayList<>();
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	//TODO : wieso machen wir dies nicht einfach mit der Person?
	public List<AdresseDO> getAdresseList() {
		return adresseList;
	}

	//TODO : wieso machen wir dies nicht einfach mit der Person?
	public void setAdresseList(List<AdresseDO> adresseList) {
		List<AdresseDO> oldAdresseList = this.adresseList;
		this.adresseList = adresseList;
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "list", oldAdresseList, this.adresseList));
	}

	public AdresseDO getSelAdresse() {
		return selAdresse;
	}

	public void setSelAdresse(AdresseDO adresse) {
		AdresseDO oldAdresse = this.selAdresse;
		this.selAdresse = adresse;
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selAdresse", oldAdresse, this.selAdresse));
	}

	public void addAdresseToList(AdresseDO adresse, PersonDO owner) {

		if (owner != null) {
			if (!owner.getPersonAdresseList().contains(adresse)) {
				owner.getPersonAdresseList().add(adresse);
				if (adresse.getAdresseBesitzer() != owner) {
					adresse.setAdresseBesitzer(owner);
				}
			}
		}
		
		//TODO : warum brauchen wir noch die this.adresseList ????
		this.adresseList = owner.getPersonAdresseList();
		this.selAdresse = adresse;

		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "list", null, adresse));
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selAdresse", null, this.selAdresse));
	}

	public void updateAdresseList(AdresseDO adresse) {
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "list", null, adresse));
	}

	public void deleteAdresseFromList(AdresseDO adresse) {
		this.adresseList.remove(adresse);
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "list", adresse, null));
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}
}
