package org.lampros.person.adresse;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.lampros.person.data.PersonDO;
import org.lampros.person.view.PersonController;

public class AdresseView extends JPanel {

	private AdresseController adresseController;
	private AdresseTableModel adresseTableModel;
	private PersonController personController;
	private AdresseDialog adresseDialog;
	private JLabel vorhandeneAdresseLabel;
	private JTable adresseTable;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JButton newAdresse;
	private JButton updateAdresse;
	private JButton deleteAdresse;

	public AdresseController getAdresseController() {
		return adresseController;
	}

	public void setAdresseController(AdresseController adresseController) {
		this.adresseController = adresseController;
	}

	public PersonController getPersonController() {
		return personController;
	}

	public void setPersonController(PersonController personController) {
		this.personController = personController;
	}

	public JTable getAdresseTable() {
		return adresseTable;
	}
	
	public AdresseDialog getAdresseDialog() {
		return adresseDialog;
	}

	public AdresseTableModel getAdresseTableModel() {
		return adresseTableModel;
	}

	public void adresseMakeView() {

		this.setLayout(new BorderLayout());

		this.vorhandeneAdresseLabel = new JLabel("Vorhandene Adresse");
		this.add(vorhandeneAdresseLabel, BorderLayout.NORTH);

		this.adresseTableModel = new AdresseTableModel(this.adresseController);
		this.adresseTable = new JTable(adresseTableModel);
		this.adresseTable.setFont(new Font("Calibri", Font.PLAIN, 14));
		this.scrollPane = new JScrollPane(adresseTable);
		this.scrollPane.setBorder(BorderFactory.createEmptyBorder());
		this.add(scrollPane, BorderLayout.CENTER);

		this.panel = new JPanel();
		this.newAdresse = new JButton("Neue Adresse");
		this.updateAdresse = new JButton("Update");
		this.deleteAdresse = new JButton("Loeschen");
		this.panel.add(newAdresse);
		this.panel.add(updateAdresse);
		this.panel.add(deleteAdresse);
		this.add(panel, BorderLayout.SOUTH);

		final AdresseController adresseTableController = getAdresseController();

		adresseTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (adresseTable.getSelectedRow() >= 0) {
					adresseTableController.getAdresseModel().setSelAdresse(adresseTableController.getAdresseModel()
							.getAdresseList().get(adresseTable.getSelectedRow()));
				}
			}
		});

		adresseTableController.getAdresseModel().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent a) {
				if ("list".equals(a.getPropertyName())) {
					adresseTableModel.fireTableDataChanged();
				}
			}
		});

		newAdresse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				PersonDO person = adresseController.getPersonController().getPersonModel().getSelectedPerson();

				if(person != null){
					adresseTableController.addAdresse();
					adresseDialog = new AdresseDialog();
					adresseDialog.setAdresseController(adresseTableController);
					adresseDialog.adresseDialogView();
				}else{
					JOptionPane.showMessageDialog(newAdresse, "Bitte wählen Sie eine Person aus", 
													"Meldung", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});

		updateAdresse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				PersonDO person = adresseController.getPersonController().getPersonModel().getSelectedPerson();
				AdresseDO adresse = adresseTableController.getAdresseModel().getSelAdresse();

				if(person != null){
					if (adresse != null) {
						AdresseDialog adresseDialog = new AdresseDialog();
						adresseDialog.setAdresseController(adresseTableController);
						adresseDialog.adresseDialogView();
						adresseDialog.showAdresseDetails(adresse);
					}
				}else{
					JOptionPane.showMessageDialog(updateAdresse, "Bitte wählen Sie eine Person aus", 
							"Meldung", JOptionPane.INFORMATION_MESSAGE);
				}
				
			}
		});

		deleteAdresse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int dialog = JOptionPane.showConfirmDialog(deleteAdresse, "Sind Sie sicher?", getName(),
						JOptionPane.YES_OPTION);
				AdresseDO adresse = adresseTableController.getAdresseModel().getSelAdresse();
				
				if (adresse != null) {
					if (dialog == JOptionPane.YES_OPTION) {
						adresseTableController.deleteAdresse(adresseTable.getSelectedRow());
						//TODO : nein, es darf jetzt noch nichts in der DB gemacht werden. Es k�nnte immer noch cancel bei der Person aufgerufen werden.
						//wenn sie irgendeine Persistenzaktion store/delete ausserhalb der Person machen, ist dies falsch - nur dort wird entschieden ob etwas in die DB geschrieben/gel�scht wird.
						adresseTableController.deleteAdresseFromDB();
						JOptionPane.showMessageDialog(deleteAdresse, "Die Adresse wurde geloescht");
					} else {
						JOptionPane.showMessageDialog(deleteAdresse, "Die Adresse wurde nicht geloescht");
					}
				} else {
					JOptionPane.showMessageDialog(deleteAdresse, "Bitte wahlen Sie eine Adresse aus");
				}
			}
		});
	}
}