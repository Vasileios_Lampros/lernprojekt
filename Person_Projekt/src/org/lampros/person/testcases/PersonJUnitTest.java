package org.lampros.person.testcases;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.*;
import java.text.ParseException;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.lampros.person.adresse.AdresseDO;
import org.lampros.person.data.PersonDAOImpl;
import org.lampros.person.data.PersonDO;
import org.lampros.person.data.PersonService;

public class PersonJUnitTest {

	private PersonDAOImpl personDAOImpl;
	private TestRunner testRunner;

	@Before
	public void setUp() throws Exception {
		personDAOImpl = new PersonDAOImpl();
		testRunner = new TestRunner();
	}

	@Test
	@Ignore
	public void testaddPerson() throws ParseException {

		PersonDO newPerson = PersonService.createPerson("Vasileios", "Lampros");
		AdresseDO newAdresse = AdresseDO.createAdresseDO("Haus", "Vaihinger Landstrasse", "8");
		
		newPerson.setIsVerheiratet("Ledig");
		newAdresse.setPostleitzahl("70195");
		newAdresse.setOrt("Stuttgart");
		PersonDO newPersonInserted = personDAOImpl.addPerson(newPerson);
		AdresseDO newAdresseInserted = personDAOImpl.addAdresse(newAdresse);

		assertThat(newPerson, isA(PersonDO.class));
		assertEquals(newPerson.getVorname(), newPersonInserted.getVorname());
		assertEquals(newPerson.getNachname(), newPersonInserted.getNachname());
		assertEquals(newPerson.getIsVerheiratet(), newPersonInserted.getIsVerheiratet());
		assertEquals(newPerson.getGeburtsdatum(), newPersonInserted.getGeburtsdatum());
		assertEquals(newAdresse.getAdresseBezeichnung(), newAdresseInserted.getAdresseBezeichnung());
		assertEquals(newAdresse.getStrasse(), newAdresseInserted.getStrasse());
		assertEquals(newAdresse.getNummer(), newAdresseInserted.getNummer());
		assertEquals(newAdresse.getPostleitzahl(), newAdresseInserted.getPostleitzahl());
		assertEquals(newAdresse.getOrt(), newAdresseInserted.getOrt());
	}

	@Test
	@Ignore
	public void testUpdatePerson() throws ParseException {

		AdresseDO oldAdresse = AdresseDO.createAdresseDO("Haus", "Vaihinger Landstrasse", "8");
		PersonDO oldPerson = personDAOImpl.addPerson(PersonService.createPerson("Vasileios", "Lampros"));

		String vorname = oldPerson.getVorname();
		String nachname = oldPerson.getNachname();
		String familienStand = oldPerson.getIsVerheiratet();
		Calendar geburtsDatum = oldPerson.getGeburtsdatum();
		String bezeichnung = oldAdresse.getAdresseBezeichnung();
		String strasse = oldAdresse.getStrasse();
		String hausNummer = oldAdresse.getNummer();

		PersonDO expectedPerson = personDAOImpl.updatePerson(oldPerson);
		AdresseDO expectedAdresse = personDAOImpl.updateAdresse(oldAdresse);
		assertThat(expectedPerson, isA(PersonDO.class));
		assertNotEquals(vorname, expectedPerson.getNachname());
		assertNotEquals(nachname, expectedPerson.getNachname());
		assertNotEquals(familienStand, expectedPerson.getIsVerheiratet());
		assertNotEquals(geburtsDatum, expectedPerson.getGeburtsdatum());
		assertNotEquals(bezeichnung, expectedAdresse.getAdresseBezeichnung());
		assertNotEquals(strasse, expectedAdresse.getStrasse());
		assertNotEquals(hausNummer, expectedAdresse.getNummer());
	}

	@Test
	@Ignore
	public void testDeletePerson() throws ParseException {

		PersonDO personToBeDeleted = personDAOImpl.addPerson(PersonService.createPerson("Vasileios", "Lampros"));
		assertTrue(personDAOImpl.deletePerson(personToBeDeleted.getPersonId()));
	}
	

	@Test
	@Ignore
	public void testSelectAllPersons() {

		personDAOImpl.selectAllPersons();
		assertFalse(personDAOImpl.selectAllPersons().size() > 8);
	}
	
	@Test
	@Ignore
	public void testUpdateAdresse() {

		AdresseDO oldAdresse = AdresseDO.createAdresseDO("Haus", "Vaihinger Landstrasse", "8");

		String bezeichnung = oldAdresse.getAdresseBezeichnung();
		String strasse = oldAdresse.getStrasse();
		String hausNummer = oldAdresse.getNummer();

		AdresseDO expectedAdresse = personDAOImpl.updateAdresse(oldAdresse);
		assertNotEquals(bezeichnung, expectedAdresse.getAdresseBezeichnung());
		assertNotEquals(strasse, expectedAdresse.getStrasse());
		assertNotEquals(hausNummer, expectedAdresse.getNummer());
	}

	@After
	public void tearDown() throws Exception {
	}
}
