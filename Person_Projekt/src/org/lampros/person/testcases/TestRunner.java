package org.lampros.person.testcases;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.lampros.person.data.PersonDAOImpl;

public class TestRunner {
	
	public TestRunner(){
		Result result = JUnitCore.runClasses(PersonDAOImpl.class);
		
		for(Failure failure : result.getFailures()){
			System.out.println(failure.toString());
		}
	}
}