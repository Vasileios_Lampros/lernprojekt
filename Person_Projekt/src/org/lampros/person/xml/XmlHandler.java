package org.lampros.person.xml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.lampros.person.data.PersonDO;
import org.lampros.person.data.Personen;

public class XmlHandler {
	
	public static void marshal(List<PersonDO> personenList, File selectedFile) throws IOException, JAXBException {
		
		BufferedWriter bw = null;
		bw = new BufferedWriter(new FileWriter(selectedFile));
		JAXBContext context = JAXBContext.newInstance(Personen.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(new Personen(personenList), bw);
		bw.close();
	}
}
