package org.lampros.person.main;

import org.lampros.person.view.PersonController;
import org.lampros.person.view.PersonModel;
import org.lampros.person.view.PersonView;

public class PersonLauncher {
	
	PersonController personController;
	
	public static void main(String[] args) {
		
		PersonLauncher personLauncher = new PersonLauncher();
		personLauncher.startApp();
	}
	
	public void startApp() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowView();
			}
		});
	}

	private void createAndShowView() {
		personController = new PersonController(new PersonModel(), new PersonView());
		personController.createView();
	}

}
