package org.lampros.person.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import org.lampros.person.data.PersonDO;

public class PersonTableView extends JPanel implements ViewInteractingInterface {

	private PersonController controller;
	private PersonTableAdapter tableAdapter;
	private JTable table;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JButton newPerson;
	private JButton delete;
	private JButton exportToXML;

	public PersonController getController() {
		return controller;
	}

	public void setPersonController(PersonController controller) {
		this.controller = controller;
	}

	public JTable getTable() {
		return table;
	}

	public PersonTableAdapter getTableAdapter() {
		return tableAdapter;
	}

	@Override
	public void createView() {

		tableAdapter = new PersonTableAdapter(this.controller);
		this.setLayout(new BorderLayout());
		this.table = new JTable(tableAdapter);
		this.table.setBounds(50, 50, 600, 300);
		this.table.setFont(new Font("Calibri", Font.PLAIN, 14));
		this.scrollPane = new JScrollPane(table);
		this.scrollPane.setBorder(BorderFactory.createEmptyBorder());
		this.add(scrollPane, BorderLayout.CENTER);

		this.panel = new JPanel(new FlowLayout());
		this.panel.setAlignmentX(CENTER_ALIGNMENT);
		this.newPerson = new JButton("Neue Person");
		this.delete = new JButton("Loeschen");
		this.exportToXML = new JButton("XML Datei");
		this.panel.add(newPerson);
		this.panel.add(delete);
		this.panel.add(exportToXML);
		this.add(panel, BorderLayout.SOUTH);

		final PersonController tableController = getController();

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (table.getSelectedRow() >= 0) {
					tableController.getPersonModel().setSelectedPerson(
							tableController.getPersonModel().getPersonList().get(table.getSelectedRow()));
				}
			}
		});

		tableController.getPersonModel().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("personList".equals(evt.getPropertyName())) {
					tableAdapter.fireTableDataChanged();
				}
			}
		});

		newPerson.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tableController.addPerson();
			}
		});

		delete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				PersonDO person = tableController.getPersonModel().getSelectedPerson();

				if (person != null) {
					int dialogButton = JOptionPane.showConfirmDialog(delete, "Sind Sie Sicher?", getName(),
							JOptionPane.YES_OPTION);
					if (dialogButton == JOptionPane.YES_OPTION) {
						tableController.deletePersonFromList(table.getSelectedRow());
						//TODO : wieso wird die Adresstabelle unsichtbar gemacht?
						tableController.getAdresseController().getAdresseView().getAdresseTable().setVisible(false);
						JOptionPane.showMessageDialog(delete, "Die ausgewalte Person wurde geloescht.");
					} else {
						JOptionPane.showMessageDialog(delete, "Die ausgewalte Person wurde nicht geloescht.");
					}
				} else {
					JOptionPane.showMessageDialog(delete, "Bitte wahlen Sie eine Person aus");
				}

			}
		});

		exportToXML.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				tableController.createXMLFile();
			}
		});
	}
}

class PersonTableAdapter extends AbstractTableModel {

	private List<ColumnDefinitions> columnNames = new ArrayList<>();
	private PersonController controller;

	public PersonTableAdapter(PersonController personControllerParam) {
		this.controller = personControllerParam;
		columnNames.add(new ColumnDefinitions("Vorname", String.class));
		columnNames.add(new ColumnDefinitions("Nachname", String.class));
		columnNames.add(new ColumnDefinitions("Bezeichnung", String.class));
		columnNames.add(new ColumnDefinitions("Strasse", String.class));
		columnNames.add(new ColumnDefinitions("Nummer", String.class));
		columnNames.add(new ColumnDefinitions("PLZ", String.class));
		columnNames.add(new ColumnDefinitions("Ort", String.class));
	}

	@Override
	public String getColumnName(int column) {
		return this.columnNames.get(column).name;
	}

	@Override
	public int getRowCount() {
		return this.controller.getPersonModel().getPersonList().size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.size();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnNames.get(columnIndex).type;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		PersonDO person = this.controller.getPersonModel().getPersonList().get(rowIndex);

		switch (columnIndex) {
		case 0:
			return person.getVorname();
		case 1:
			return person.getNachname();
		case 2:
			return person.getAdresseBezeichnung();
		case 3:
			return person.getStrasse();
		case 4:
			return person.getNummer();
		case 5:
			return person.getPostleitzahl();
		case 6:
			return person.getOrt();
		default:
			break;
		}
		return null;
	}

	@Override
	public void setValueAt(Object newValue, int rowIndex, int columnIndex) {

		PersonDO person = this.controller.getPersonModel().getPersonList().get(rowIndex);

		switch (columnIndex) {
		case 0:
			person.setVorname(this.controller.getPersonModel().getPersonList().get(rowIndex).getVorname());
		case 1:
			person.setNachname(this.controller.getPersonModel().getPersonList().get(rowIndex).getNachname());
		case 2:
			person.setAdresseBezeichnung((String) newValue);
		case 3:
			person.setStrasse((String) newValue);
		case 4:
			person.setNummer((String) newValue);
		case 5:
			person.setPostleitzahl((String) newValue);
		case 6:
			person.setOrt((String) newValue);
		default:
			break;
		}
	}
}

class ColumnDefinitions {
	String name;
	Class<?> type;

	ColumnDefinitions(String nameParam, Class<?> typeParam) {
		this.name = nameParam;
		this.type = typeParam;
	}

}