package org.lampros.person.view;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

import org.lampros.person.data.PersonDO;

public class PersonModel {

	private List<PersonDO> personList = new ArrayList<>();
	private PersonDO selectedPerson;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public List<PersonDO> getPersonList() {
		return personList;
	}

	public PersonDO getSelectedPerson() {
		return selectedPerson;
	}

	public void setSelectedPerson(PersonDO selectedPerson) {
		PersonDO oldPerson = this.selectedPerson;
		this.selectedPerson = selectedPerson;
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selectedPerson", oldPerson, this.selectedPerson));
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.pcs.removePropertyChangeListener(listener);
	}

	public void addPersonToList(PersonDO person) {
		if (this.personList.contains(person)) {
			return;
		}

		this.personList.add(person);
		this.selectedPerson = person;

		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "personList", null, person));
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "selectedPerson", null, this.selectedPerson));
	}

	public void deletePerson(PersonDO personDO) {
		this.personList.remove(personDO);
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "personList", personDO, null));
	}

	public void updatePersonList(PersonDO personDO) {
		this.pcs.firePropertyChange(new PropertyChangeEvent(this, "personList", null, personDO));
	}
}