package org.lampros.person.view;

import java.awt.HeadlessException;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;

import org.lampros.person.adresse.AdresseController;

public class PersonView extends JFrame implements ViewInteractingInterface {

	private PersonController personController;
	private AdresseController adresseController;
	private PersonTableView master;
	private JLabel tableTitel;

	public PersonView() {
		super("Person Projekt");
		try {
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		} catch (HeadlessException he) {
			he.printStackTrace();
		}
	}

	public PersonController getPersonController() {
		return personController;
	}

	public void setPersonController(PersonController personController) {
		this.personController = personController;
	}

	public void setAdresseController(AdresseController adresseController) {
		this.adresseController = adresseController;
	}

	public PersonTableView getMaster() {
		return master;
	}
	
	public void showView() {
		pack();
		this.setVisible(true);
		this.setSize(800, 700);
	}

	@Override
	public void createView() {

		Box box = Box.createVerticalBox();

		this.tableTitel = new JLabel("Adressbuch");
		this.tableTitel.setAlignmentY(CENTER_ALIGNMENT);
		box.add(tableTitel);

		master = new PersonTableView();
		master.setPersonController(this.personController);
		master.createView();
		box.add(master);

		JSeparator tableViewSeparator = new JSeparator();
		box.add(tableViewSeparator);

		PersonDetailView detail = new PersonDetailView();
		detail.setPersonController(this.personController);
		//TODO : warum nochmal separat den adresscontroller reingeben, wenn der personcontroller ihn schon hat ???
		detail.setAdresseController(this.adresseController);
		detail.createView();
		box.add(detail);

		this.add(box);
	}
}