package org.lampros.person.view;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

import org.lampros.person.adresse.AdresseController;
import org.lampros.person.adresse.AdresseModel;
import org.lampros.person.adresse.AdresseView;
import org.lampros.person.data.PersonDAOImpl;
import org.lampros.person.data.PersonDO;
import org.lampros.person.data.PersonService;
import org.lampros.person.xml.XmlHandler;

public class PersonController implements ViewInteractingInterface {

	private PersonModel personModel;
	private PersonView personView;
	private AdresseController adresseController = new AdresseController(new AdresseModel(), new AdresseView());

	public PersonController(PersonModel personModel, PersonView personView) {
		this.personView = personView;
		this.personModel = personModel;
		this.personView.setPersonController(this);
		//TODO : warum nochmal separat den adresscontroller reingeben, wenn der personcontroller ihn schon hat ???
		this.personView.setAdresseController(this.adresseController);
		this.adresseController.setPersonController(this);
	}

	public AdresseController getAdresseController() {
		return adresseController;
	}

	public PersonView getPersonView() {
		return personView;
	}

	public PersonModel getPersonModel() {
		return personModel;
	}

	@Override
	public void createView() {
		ladenInitialData();
		this.personView.createView();
		this.personView.showView();
		this.adresseController.createView();
	}

	public void ladenInitialData() {
		//TODO : was soll dass? Mit einer Persistenzanbindung gibt es keine initialen Daten mehr
		//stattdessen sollen die gespeicherten Daten in der DB geladen werden.
		this.personModel.getPersonList().addAll(initialData());
	}

	private List<PersonDO> initialData() {
	
		List<PersonDO> initialPersons = new ArrayList<>();
		initialPersons.add(PersonService.createPerson("Vorname 1", "Nachname 1"));
		initialPersons.add(PersonService.createPerson("Vorname 2", "Nachname 2"));
		initialPersons.add(PersonService.createPerson("Vorname 3", "Nachname 3"));
		initialPersons.add(PersonService.createPerson("Vorname 4", "Nachname 4"));

		return initialPersons;
	}

	public PersonDO addPerson() {
		PersonDO person = PersonService.createPerson(null, null);
		this.personModel.addPersonToList(person);
		return person;
	}

	public void deletePersonFromList(int personIndex) {
		//TODO: warum jedes mal ein neues DAO instanzieren, nochmal nachdenken wo es am besten aufgehoben ist, dito bei anderen Methoden 
		PersonDAOImpl dao = new PersonDAOImpl();
		dao.deletePerson(this.personModel.getSelectedPerson().getPersonId());
		this.personModel.deletePerson(this.personModel.getPersonList().get(personIndex));
	}

	public void savePerson(PersonDO person) {
		this.personModel.updatePersonList(person);
		PersonDAOImpl dao = new PersonDAOImpl();
		dao.addPerson(person);
		this.personModel.setSelectedPerson(null);
	}

	public void updatePersonDetails(PersonDO person) {
		this.personModel.updatePersonList(person);
		PersonDAOImpl dao = new PersonDAOImpl();
		dao.updatePerson(person);
		this.personModel.setSelectedPerson(null);
	}

	public void cancelAdding(PersonDO person) {
		this.personModel.setSelectedPerson(null);
	}

	public void createXMLFile() {
		try {
			XmlHandler.marshal(new PersonDAOImpl().selectAllPersons(), new File("xmlFile.xml"));
			JOptionPane.showMessageDialog(null, "Die Datei wurde erfolgreich erzeugt");
		} catch (Exception fileExc) {
			JOptionPane.showMessageDialog(null, "Die Datei wurde nicht erzeugt");
			fileExc.printStackTrace();
		}
	}
}