package org.lampros.person.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.lampros.person.adresse.AdresseController;
import org.lampros.person.adresse.AdresseDO;
import org.lampros.person.adresse.AdresseView;
import org.lampros.person.data.PersonDO;
import org.lampros.person.data.PersonService;

public class PersonDetailView extends JPanel implements ViewInteractingInterface {

	private PersonController personController;
	private AdresseController adresseController;
	private JPanel northPanel;
	private JLabel vornameLabel;
	private JTextField vornameField;
	private JLabel nachnameLabel;
	private JTextField nachnameField;
	private JLabel familienStandLabel;
	private JComboBox<String> isVerheiratetBox;
	private JLabel geburtsDatumLabel;
	private JTextField geburtsDatumField;
	private AdresseView adresseView;
	private JPanel southPanel;
	private JButton save;
	private JButton cancel;
	private JSeparator separator1;
	private JSeparator separator2;

	public PersonController getPersonController() {
		return personController;
	}

	public void setPersonController(PersonController controller) {
		this.personController = controller;
	}

	public AdresseController getAdresseController() {
		return adresseController;
	}

	public void setAdresseController(AdresseController adresseController) {
		this.adresseController = adresseController;
	}

	public AdresseView getAdresseView() {
		return adresseView;
	}

	@Override
	public void createView() {

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.northPanel = new JPanel(new GridLayout(0, 2));
		this.vornameLabel = new JLabel("Vorname");
		this.northPanel.add(vornameLabel);

		this.vornameField = new JTextField();
		this.northPanel.add(vornameField);

		this.nachnameLabel = new JLabel("Nachname");
		this.northPanel.add(nachnameLabel);

		this.nachnameField = new JTextField();
		this.nachnameField.selectAll();
		this.northPanel.add(nachnameField);

		this.familienStandLabel = new JLabel("Familienstand");
		this.northPanel.add(familienStandLabel);

		String[] values = { "-", "Verheiratet", "Ledig", "Keine Angabe" };
		this.isVerheiratetBox = new JComboBox<>(values);
		this.northPanel.add(isVerheiratetBox);

		this.geburtsDatumLabel = new JLabel("Geburtsdatum");
		this.northPanel.add(geburtsDatumLabel);

		this.geburtsDatumField = new JTextField();
		this.northPanel.add(geburtsDatumField);

		this.add(northPanel, BorderLayout.NORTH);

		this.separator1 = new JSeparator();
		this.add(separator1);

		this.add(this.adresseController.getAdresseView(), BorderLayout.CENTER);

		separator2 = new JSeparator();
		this.add(separator2);

		this.southPanel = new JPanel();
		this.save = new JButton("Speichern");
		this.southPanel.add(save);
		this.cancel = new JButton("Cancel");
		this.southPanel.add(cancel);
		this.add(southPanel, BorderLayout.SOUTH);

		final PersonController panelController = getPersonController();
		final AdresseController adresseController = getAdresseController();

		panelController.getPersonModel().addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				if ("selectedPerson".equals(evt.getPropertyName())) {
					if (panelController.getPersonModel().getSelectedPerson() != null) {
						panelController.getAdresseController().getAdresseView().getAdresseTable().setVisible(true);
						clearAdresseTable();
						panelController.getAdresseController().getAdresseModel().setAdresseList(
								panelController.getPersonModel().getSelectedPerson().getPersonAdresseList());
						updateControl(panelController.getPersonModel().getSelectedPerson());
					} else {
						clearValuesFromPersonDetailBereich();
						panelController.getAdresseController().getAdresseView().getAdresseTable().setVisible(false);
					}
				}
			}
		});

		//TODO : wofür wird dies gebraucht ???
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addPropertyChangeListener("permanentFocusOwner",
				new PropertyChangeListener() {
					@Override
					public void propertyChange(final PropertyChangeEvent e) {
						if (e.getOldValue() instanceof JTextField) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									JTextField oldTextField = (JTextField) e.getOldValue();
									oldTextField.setSelectionStart(0);
									oldTextField.setSelectionEnd(0);
								}
							});
						}
						if (e.getNewValue() instanceof JTextField) {
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									JTextField jTextField = (JTextField) e.getNewValue();
									jTextField.selectAll();
								}
							});
						}
					}
				});

		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				PersonDO person = panelController.getPersonModel().getSelectedPerson();
				AdresseDO adresse = adresseController.getAdresseModel().getSelAdresse();

				if (person != null)
					try {
						if (geburtsDatumField.getText().isEmpty()
								|| (PersonService.getDateAsCalendar(geburtsDatumField.getText())
										.after(PersonService.getDateAsCalendar("01.01.1970")))
										&& (geburtsDatumField.getText()
												.matches("[0-9][0-9][\\.][0-9][0-9][\\.][0-9][0-9][0-9][0-9]")
												|| geburtsDatumField.getText()
														.matches("[0-9][\\.][0-9][\\.][0-9][0-9][0-9][0-9]"))) {
							try {
								updateModel(person, adresse);
							} catch (ParseException e1) {
								//TODO : was passiert jetzt, ein Fehler und der Benutzer merkt davon nichts, dies kann nicht sein -> Fehlerhandling überarbeiten.
								e1.getStackTrace();
							}

							if ((person.getPersonId() == 0) && (adresse.getAdresseId() == 0)) {
								panelController.savePerson(person);
							} else {
								panelController.updatePersonDetails(person);
								if (adresse != null) {
									adresseController.updateAdresseDetails(adresse);
								}
							}
							clearValuesFromPersonDetailBereich();
						} else {
							JOptionPane.showMessageDialog(null, "Bitte geben Sie einen gultigen Datum ein", "Fehler",
									JOptionPane.ERROR_MESSAGE);
							geburtsDatumField.setText("");
							//TODO : was passiert hier und warum???
							if (panelController.getPersonModel().getSelectedPerson().getPersonAdresseList().isEmpty()) {
								adresseController.deleteAdresse(
										adresseController.getAdresseView().getAdresseTable().getRowCount() - 1);
							}
						}
					} catch (HeadlessException | ParseException e1) {
						//TODO : was passiert jetzt, ein Fehler und der Benutzer merkt davon nichts, dies kann nicht sein -> Fehlerhandling überarbeiten.
						e1.getStackTrace();
					}
			}
		});

		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelController.cancelAdding(panelController.getPersonModel().getSelectedPerson());
				clearValuesFromPersonDetailBereich();
				adresseController.deleteAdresse(adresseController.getAdresseView().getAdresseTable().getRowCount() - 1);
			}
		});
	}

	private void updateControl(PersonDO person) {
		vornameField.setText(person.getVorname());
		nachnameField.setText(person.getNachname());
		isVerheiratetBox.setSelectedItem(person.getIsVerheiratet());
		geburtsDatumField.setText(PersonService.getDateAsString(person.getGeburtsdatum()));
		personController.getAdresseController().retrieveAdresseFromDB(person);
	}

	private void updateModel(PersonDO person, AdresseDO adresse) throws ParseException {
		person.setGeburtsdatum(PersonService.getDateAsCalendar(geburtsDatumField.getText()));
		person.setVorname(vornameField.getText());
		person.setNachname(nachnameField.getText());
		person.setIsVerheiratet((String) isVerheiratetBox.getSelectedItem());
		person.setAdresseBezeichnung(adresse.getAdresseBezeichnung());
		person.setStrasse(adresse.getStrasse());
		person.setNummer(adresse.getNummer());
		person.setPostleitzahl(adresse.getOrt());
		person.setOrt(adresse.getPostleitzahl());
	}

	private void clearValuesFromPersonDetailBereich() {
		vornameField.setText("");
		nachnameField.setText("");
		isVerheiratetBox.setSelectedIndex(0);
		geburtsDatumField.setText("");
	}

	public void clearAdresseTable() {
		for (int i = 0; i < this.adresseController.getAdresseView().getAdresseTableModel().getRowCount(); i++) {
			for (int j = 0; j < this.adresseController.getAdresseView().getAdresseTableModel().getColumnCount(); j++) {
				this.adresseController.getAdresseView().getAdresseTableModel().setValueAt("", i, j);
			}
		}
	}
}
