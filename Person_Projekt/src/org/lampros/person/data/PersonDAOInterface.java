package org.lampros.person.data;

import java.util.List;

import org.lampros.person.adresse.AdresseDO;

public interface PersonDAOInterface {
	
	public PersonDO addPerson(PersonDO person);
	
	public boolean deletePerson(int index);
	
	public PersonDO updatePerson(PersonDO person);
	
	public List<PersonDO> selectAllPersons();
	
	public AdresseDO addAdresse(AdresseDO adresse);
	
	public boolean deleteAdresse(int index);
	
	public AdresseDO updateAdresse(AdresseDO adresse);
	
	public List<AdresseDO> retrieveAdresse(PersonDO person);
}
