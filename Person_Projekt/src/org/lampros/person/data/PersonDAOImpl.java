package org.lampros.person.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.lampros.person.adresse.AdresseDO;

public class PersonDAOImpl implements PersonDAOInterface {
	//TODO : es kann nicht sein, dass eine Exception passiert und der Benutzer nichts mitbekommt.
	//Exception propagieren nach oben und dem Benutzer anzeigen
	//TODO : In einem PersonDAO sollen nur Aktionen zu PersonDO passieren. AdresseDO soll eigenes DAO haben.
	private final static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Person_Projekt");

	@Override
	public PersonDO addPerson(PersonDO person) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;

		try {
			transaction = em.getTransaction();
			transaction.begin();

			em.persist(person);

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return person;
	}

	@Override
	public PersonDO updatePerson(PersonDO updatedPerson) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;
		PersonDO person = em.find(PersonDO.class, updatedPerson.getPersonId());

		person.setVorname(updatedPerson.getVorname());
		person.setNachname(updatedPerson.getNachname());
		person.setIsVerheiratet(updatedPerson.getIsVerheiratet());
		person.setGeburtsdatum(updatedPerson.getGeburtsdatum());
		
		try {
			transaction = em.getTransaction();
			transaction.begin();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return person;
	}

	@Override
	public boolean deletePerson(int indexToDelete) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;
		PersonDO person = em.find(PersonDO.class, indexToDelete);

		try {
			transaction = em.getTransaction();
			transaction.begin();

			em.remove(person);

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return true;
	}

	@Override
	public List<PersonDO> selectAllPersons() {

		List<PersonDO> personen = null;

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;

		try {
			transaction = em.getTransaction();
			transaction.begin();

			TypedQuery<PersonDO> query = em.createQuery("Select p From PersonDO p", PersonDO.class);
			personen = query.getResultList();

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return personen;
	}

	@Override
	public AdresseDO addAdresse(AdresseDO adresse) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;

		try {
			transaction = em.getTransaction();
			transaction.begin();

			em.persist(adresse);

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return adresse;
	}

	@Override
	public AdresseDO updateAdresse(AdresseDO updatedAdresse) {

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;
		AdresseDO adresse = em.find(AdresseDO.class, updatedAdresse.getAdresseId());

		try {
			transaction = em.getTransaction();
			transaction.begin();

			adresse.setAdresseBezeichnung(updatedAdresse.getAdresseBezeichnung());
			adresse.setStrasse(updatedAdresse.getStrasse());
			adresse.setNummer(updatedAdresse.getNummer());
			adresse.setPostleitzahl(updatedAdresse.getPostleitzahl());
			adresse.setOrt(updatedAdresse.getOrt());

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}
		return adresse;
	}

	@Override
	public boolean deleteAdresse(int indexToDelete) {
		
		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;
		AdresseDO adresse = em.find(AdresseDO.class, indexToDelete);

		try {
			transaction = em.getTransaction();
			transaction.begin();

			em.remove(adresse);

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return true;
	}
	
	@Override
	public List<AdresseDO> retrieveAdresse(PersonDO person) {

		List<AdresseDO> adresseList = null;

		EntityManager em = emf.createEntityManager();
		EntityTransaction transaction = null;

		try {
			transaction = em.getTransaction();
			transaction.begin();

			Query query = em.createQuery("SELECT a FROM AdresseDO a WHERE OWNER_ID = " + person.getPersonId());
			adresseList = query.getResultList();

			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		} finally {
			em.close();
		}

		return adresseList;
	}
}
