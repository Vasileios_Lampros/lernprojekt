package org.lampros.person.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.lampros.person.adresse.AdresseDO;

@XmlRootElement(name = "PersonDO")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "id", "vorname", "nachname", "isVerheiratet", "geburtsdatum" })
@Entity
@Table(name = "PERSONEN")
public class PersonDO implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PERSON_ID", nullable = false)
	private int id;
	@Column(name = "VORNAME", nullable = false)
	private String vorname;
	@Column(name = "NACHNAME", nullable = false)
	private String nachname;
	@Transient
	@XmlTransient
	private String adresseBezeichnung;
	@Transient
	@XmlTransient
	private String strasse;
	@Transient
	@XmlTransient
	private String nummer;
	@Transient
	@XmlTransient
	private String postleitzahl;
	@Transient
	@XmlTransient
	private String ort;
	@Column(name = "GEBURTSDATUM")
	private Calendar geburtsdatum;
	@Column(name = "FAMILIENSTAND")
	private String isVerheiratet;
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "OWNER_ID", referencedColumnName = "PERSON_ID")
	@XmlTransient
	private List<AdresseDO> personAdresseList = new ArrayList<AdresseDO>();

	public List<AdresseDO> getPersonAdresseList() {
		return personAdresseList;
	}

	public void setPersonAdresseList(List<AdresseDO> personAdresseList) {
		this.personAdresseList = personAdresseList;
	}

	public int getPersonId() {
		return id;
	}

	public void setPersonId(int id) {
		this.id = id;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getAdresseBezeichnung() {
		return adresseBezeichnung;
	}

	public void setAdresseBezeichnung(String adresseBezeichnung) {
		this.adresseBezeichnung = adresseBezeichnung;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getNummer() {
		return nummer;
	}

	public void setNummer(String nummer) {
		this.nummer = nummer;
	}

	public String getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(String postleitzahl) {
		this.postleitzahl = postleitzahl;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Calendar getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(Calendar geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public String getIsVerheiratet() {
		return isVerheiratet;
	}

	public void setIsVerheiratet(String isVerheiratet) {
		this.isVerheiratet = isVerheiratet;
	}
}
