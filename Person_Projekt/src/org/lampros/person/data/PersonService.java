package org.lampros.person.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class PersonService {

	public static PersonDO createPerson(String vornameParam, String nachNameParam) {
		PersonDO personDO = new PersonDO();

		personDO.setVorname(vornameParam);
		personDO.setNachname(nachNameParam);

		return personDO;
	}

	public static String getDateAsString(Calendar date) {
		return getDateAsString(date, "dd.MM.yyyy");
	}

	public static String getDateAsString(Calendar date, String formatPattern) {
		if (date == null)
			return "";

		SimpleDateFormat df = new SimpleDateFormat(formatPattern);
		String formattedDate = df.format(date.getTime());
		return formattedDate;
	}

	public static Calendar getDateAsCalendar(String dateString) throws ParseException {
		return getDateAsCalendar(dateString, "dd.MM.yyyy");
	}

	public static Calendar getDateAsCalendar(String dateString, String formatPattern) throws ParseException {
		if (dateString.equals(""))
			return null;

		SimpleDateFormat df = new SimpleDateFormat(formatPattern);
		Date date;

		date = df.parse(dateString);
		return getCurrentCalendarFromDate(date);
	}

	public static Calendar getCurrentCalendarFromDate(Date date) {
		Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(date.getTime());
		return cal;
	}

}
