package org.lampros.person.data;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

import org.lampros.person.data.PersonDO;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Personen")
public class Personen {

	@XmlElement(name = "PersonDO", type = PersonDO.class)
	private List<PersonDO> personen = new ArrayList<PersonDO>();

	public Personen() {
	}

	public Personen(List<PersonDO> personen) {
		super();
		this.personen = personen;
	}

	public List<PersonDO> getPersonen() {
		return personen;
	}

	public void setPersonen(List<PersonDO> personen) {
		this.personen = personen;
	}
}
